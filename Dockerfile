FROM camunda/connectors-bundle:0.19.2

HEALTHCHECK --interval=30s --timeout=5s --retries=3 CMD curl -f http://localhost:8080/actuator/health/readiness || exit 1